package com.griddynamics.test;

import com.griddynamics.test.common.TestBase;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static io.restassured.http.ContentType.JSON;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author Timofei B.
 */
// @Epic
// @Feature
public class HappyPathTest extends TestBase {

    /*****************
     ***     positive
     ****************/

    @Test
    public void create() {
        //delete all greetings
        Map<String, Greeting> firstResult = when().get("/greeting")
                .then().statusCode(SC_OK)
                .extract().body().as(new TypeToken<HashMap<String, Greeting>>() {
                }.getType());

        firstResult.keySet().forEach(id -> given().param("id", id)
                .when().delete("/greeting"));
        //set parameter for new greetings
        final Map<String, Object> bodyAsMap = new HashMap<>();
        String expectedContent = "some context";
        bodyAsMap.put("content", expectedContent);
        String expectedSubtext = "some subtext";
        bodyAsMap.put("subtext", expectedSubtext);
        //execute test
        Greeting expected = given().contentType(JSON).body(bodyAsMap).
                when().post("/greeting").
                then().statusCode(SC_OK).extract()
                .body().as(Greeting.class);
    }


    // @Description
    // @Story
    @Test
    public void read() {
        when().get("/lotto").then().statusCode(SC_OK).body("lotto.lottoId", equalTo(5));
    }

    @Test
    public void update() {

        final Map<String, Object> bodyAsMap = new HashMap<>();

        given().contentType(JSON).body(bodyAsMap).
                when().put().
                then().statusCode(SC_OK);
    }

    @Test
    public void delete() {
        // TODO:
    }

    /*****************
     ***    negative
     ****************/
}
