package LuckyTicket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        LuckyTicket ticket = new LuckyTicket();

        for (int i = 0; ; i++) {
            System.out.println("Enter you ticket number in one string without empties");
            String input = reader.readLine();
            ticket = new LuckyTicket(input);
            if (ticket.checkInput()) break;
        }

        System.out.println(ticket.checkLuck());
    }
}



